# OpenML dataset: internet-firewall

https://www.openml.org/d/43039

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Fatih Ertam 
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Internet+Firewall+Data) - 2019
**Please cite**: [UCI](https://archive.ics.uci.edu/ml/citation_policy.html)  

**Internet Firewall Data Data Set**

This data set was collected from the internet traffic records on a university's firewall. There are 12 features in total. Action feature is used as a class. There are 4 classes in total. These are allow, action, drop and reset-both classes.

### Attribute information

- Source Port
- Destination Port
- NAT Source Port
- NAT Destination Port
- Action
- Bytes
- Bytes Sent
- Bytes Received
- Packets
- Elapsed Time (sec)
- pkts_sent
- pkts_received

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43039) of an [OpenML dataset](https://www.openml.org/d/43039). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43039/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43039/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43039/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

